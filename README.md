# README


Youlister is an app that allow you to store all the Youtube ID poster on IRC

With Youlister, you can see all the thumbnail of your video, the number of post related to this video, and the date of post.



Please feel free to use a different markup language if you do not plan to run
<tt>rake doc:app</tt>.



# Install : 


Install rbenv like in https://github.com/sstephenson/rbenv

sudo apt-get install nodejs


Install bundler like in https://github.com/bundler/bundler

After you close your terminal

`bundle install`

Put Nginx conf to site-enabled folder

`bundle exec rake assets:clean assets:precompile RAILS_ENV=production`

`sudo service nginx reload`

`bundle exec unicorn -E production -c config/unicorn.rb`

`sudo service nginx reload`

# For the cron

Create a folder IRC in your home
Create a folder logIDyoutube in your home

copy the youtube and copy script in your IRC folder

modify your crontab file with

`crontab -e`


add 

`*/5 *  * *  * /bin/bash -l -c /home/username/IRC/copy`

`*/5 *  * *  * /bin/bash -l -c /home/username/youlister/importandarchive`


# Utilisation 

inityoutubecount help you to make the IDCount.txt file that initialise your application

copy allow you to copy your log IRC and call youtube that make a list of ID

importandarchive import your ID.txt file and archive the file in logIDyoutube

You can do 

`rake importvideo` to import video (the crontab do that)

or

`rake initvideocount` to initialise with the IDCount.txt file (you do that just one time)