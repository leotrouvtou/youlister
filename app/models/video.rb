class Video < ActiveRecord::Base
  require 'json'
  require 'open-uri'
  has_many :logins
  before_save  :gettitle

  def gettitle
    url = "https://www.googleapis.com/youtube/v3/videos?part=snippet&id=" + self.youID + "&part=contentDetails&key=token"
    begin
    doc = JSON.parse(open(url))
    self.title=doc.items[0].snipet.title
    rescue OpenURI::HTTPError => ex
      self.title = "Handle missing video here"
    end
  end


end
