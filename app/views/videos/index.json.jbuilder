json.array!(@videos) do |video|
  json.extract! video, :id, :nom, :youID, :comment
  json.url video_url(video, format: :json)
end
