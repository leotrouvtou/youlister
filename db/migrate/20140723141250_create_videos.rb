class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :nom
      t.string :youID
      t.text :comment

      t.timestamps
    end
  end
end
