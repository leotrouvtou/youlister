class AddCountToVideos < ActiveRecord::Migration
  def change
    add_column :videos, :count, :integer
  end
end
