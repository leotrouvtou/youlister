class CreateLogins < ActiveRecord::Migration
  def change
    create_table :logins do |t|
      t.string :name
      t.integer :youID
      t.integer :count

      t.timestamps
    end
  end
end
