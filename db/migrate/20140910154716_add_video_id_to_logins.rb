class AddVideoIdToLogins < ActiveRecord::Migration
  def change
    add_column :logins, :videos, :integer
    add_index  :logins, :videos
  end
end
