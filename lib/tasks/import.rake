#lib/tasks/import.

desc :'Look for the ID in ID.txt'
task :importvideo => :environment do

  puts "import running"
  i=0
  fichier = File.open("ID.txt", "r")
  fichier.each_line { |ligne|
     params = ligne.split(' ')
     c = Module::Video.find_or_create_by(:youID=>params[0])
     c.count ||= 0
     c.count += 1
     c.touch(:updated_at)
     c.save
     i = i+1
     puts i
  }
  fichier.close
  puts "import done"
  
end

desc :'Init you video list with login and count'
task :initvideologinandcount => :environment do

  puts "import running"
  fichier = File.open("IDCountandUser.txt", "r")
  fichier.each_line { |ligne|
     params = ligne.split(' ')
     c = Module::Video.find_or_create_by(:youID=>params[0])
     c.count = params[1]
     loginliste = params[2].split(',')
     loginliste.each { |nick|
       d = Module::Login.find_or_create_by(:youID=>c.youID, :name=>nick)
       d.count=c.count/loginliste.size
       d.save
     }
     c.save
  }
  fichier.close
  puts "import done"
  
end



desc :'Init you video list with login and count'
task :initvideocount => :environment do

  puts "import running"
  fichier = File.open("IDCount.txt", "r")
  i=0
  fichier.each_line { |ligne|
     params = ligne.split(' ')
     c = Module::Video.find_or_create_by(:youID=>params[0])
     c.count = params[1]
     c.save
     sleep rand(10)
     i = i+1
     puts i
  }
  fichier.close
  puts "import done"
  
end
